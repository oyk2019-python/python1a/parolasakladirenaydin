parolalar = []  # parolalar dizisi olusturduk
while True:  # döngünün devam etmesini istiyoruz bu yüzden True diyoruz
    print('Bir işlem seçin')  # islem seci yazdır
    print('1- Parolaları Listele')  # parolaları listele yazdır
    print('2- Yeni Parola Kaydet')  # yeni parola kaydet yazdır
    islem = input('Ne Yapmak İstiyorsun :')  # inputla ne yapmak istediğini alıyoruz bunu isleme tanımlıyoruz
    if islem.isdigit():  # islem rakam mi degil mi
        islem_int = int(islem)  # int degerse islem_int atıyrouz
        if islem_int not in [1, 2]:  # girilen deger 1 le 2 arasında degilse
            print('Hatalı işlem girişi')  # hatali islem girdisi yazdir
            continue  # isleme devam ediyoruz
        if islem_int == 2:  # 2.islemi sectiysek

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')  # inputla girdi_ismi
            kullanici_adi = input('Kullanici Adi Girin :')  # inputla kullanici_adi
            parola = input('Parola :')  # inputla parola
            parola2 = input('Parola Yeniden :')  # inputla parola2
            eposta = input('Kayitli E-posta :')  # inputla eposta
            gizlisorucevabi = input('Gizli Soru Cevabı :')  # inputla gizlisorucevabi alıyoruz
            if kullanici_adi.strip() == '':  # burada iflerin icinde bos deger mi onları kontrol ediyoruz
                print('kullanici_adi girmediz')  # eger bossa bastan baslatıyoruz
                continue  # continue ile devam ediyoruz
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:  # parola esit degilse printle bastırıyoruz
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {  # yeni_girdi adinda sözlükte key value degerlerini tutuyoruz. Value degerlerini yukarıda aldık.
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)  # parolalar adlı dizimize sözlügümüzü ekliyoruz
            continue
        elif islem_int == 1:  # islem 1 sectiysek
            alt_islem_parola_no = 0  # alt_islem_parola_no 0 degerini verdik
            for parola in parolalar:  # parola parolaların icindeyse
                alt_islem_parola_no += 1  # alt_islem_parola_noyu 1 arttır
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no,
                                                     girdi=parola.get('girdi_ismi')))  # value degerlerini yazdırıyoruz
            alt_islem = input('Yukarıdakilerden hangisi ?: ')  # inputla islem secmesini istiyoruz
            if alt_islem.isdigit():  # alt_islem rakam mı degil mi
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(
                        alt_islem):  # basılan rakam - deger veya 2 den büyükse hatalı parola secimi yazdırıyoruz
                    print('Hatalı parola seçimi')
                    continue
                parola = parolalar[int(
                    alt_islem) - 1]  # alt_islem degerini 1 den basladigi icin 1 azaltip parolalar dizine ekleyip parolaya esitledik
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))  # formatla key value degerlerini yazdiriyoruz
                continue

    print('Hatalı giriş yaptınız')  # hatalı rakam deger sectigimizde yazdırılacak
